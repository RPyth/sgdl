
# SGDL - Simple Graphical Description Language
**SGDL** is a standard capable of defining simple raster graphics, such as spritework and pixel art.
## Usage and Conversion
This repository offers *guisgdl.py* as a SGDL-enabled text editor with a number of handy features like color-based highlighting, real-time visualizer and various editing tools. Compatible with Python 3.10 and higher, depends on `pillow` as the only non-standard library.
## Basic Syntax
Tag declaration always begins with a `#`, sub-tag declaration is handled by `:` in some tags.
Example:
> #sgdl #colors red: (255, 0, 0, 255) 

The `#sgdl` tag is required and may hold current version of the standard (1.02). Other obligatory tags include `#width`,`#colors` and `#pixels`.
Optional tags include `#background`, `#layers`, `#duration`, `#scale` along with all user-defined tags.
Minimal SGDL example:
`#sgdl #width 1 #colors red:f00 #pixels red`
Comments must be marked by `/` before and after the comment, due to which division is handled by `\` instead.

## Tags
- `#sgdl` - obligatory tag, may contain standard version
- `#width` - obligatory tag, value must be an integer, height is defined dynamically based on pixel area and width
- `#colors` - obligatory tag, color names can vary in length, though single-letter values are preferred, color value can be defined in hex or as a four-tuple
- `#pixels` - refers to color names defined in `#colors`, unrecognized symbols are ignored, newline and comma separation is supported, along with "dynamic" pixels
- `#layers` - overrides the `#pixels` tag as the one and only layer, must contain space-separated layer names, which must be present in the file as tags that begin with a `#`
- `#duration` - value must be an integer, that represents duration of each frame/layer in milliseconds, when omitted, layers overlay each other instead of appearing in a sequence
- `#background` - optional tag, assumed to be (0, 0, 0, 0) when omitted
- `#scale` - optional tag, value is an integer, which defines x and y dimensions of one pixel
- `#modifier` - optional tag, defines the brightness modification constant
## Static Pixel Operations
Once surrounded with parenthesis, a color sequence can be multiplied using a value or an expression.
Examples:
> #pixels (red)*100

Same as above, but more understandable for a 10x10 image:
> #pixels (red)\*10\*10

Transparency can be called by `_` even if not defined as a color.

Pixel brightness can be modified by placing `+` or `-` right before the pixel, several same symbols in a row make the effect stronger.
## Dynamic Pixels

Are usually represented by keywords surrounded by brackets, but may have a shortened representation:
- `[blur]` or `=`, has intermediate value compared to surrounding horizontal pixels
- `[vblur]` or `|`, same as above, but vertical
- `[lcopy]` or `<`, copies the value to the left
- `[rcopy]` or `>`, copies the value to the right
- `[fill]` or `.`, fills the rest of the line with transparency
- `[fun 0,0,0,0]`, unique function-based color selection pixel value, uses variable "place" which holds the pixel position

> Written with [StackEdit](https://stackedit.io/).